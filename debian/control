Source: grapefruit
Section: python
Priority: optional
Maintainer: Debian Python Modules Team <python-modules-team@lists.alioth.debian.org>
Uploaders: Jonathan Carter <jcc@debian.org>
Build-Depends:
 debhelper (>= 11),
 dh-python,
 python-all,
 python-setuptools,
 python3-all,
 python3-setuptools,
 python-pytest,
 python3-pytest,
 python3-sphinx,
 python-future,
 python3-future,
Standards-Version: 4.1.4
Homepage: https://github.com/xav/grapefruit
Vcs-Git: https://salsa.debian.org/python-team/modules/grapefruit.git
Vcs-Browser: https://salsa.debian.org/python-team/modules/grapefruit
Testsuite: autopkgtest-pkg-python

Package: python-grapefruit
Architecture: all
Depends: ${misc:Depends}, ${python:Depends}
Suggests: python-grapefruit-doc
Description: Python module to manipulate color information easily (Python 2)
 GrapeFruit is a pure Python module that lets you easily manipulate and convert
 color information. Its primary goal is to be natural and flexible.
 .
 The following color systems are supported by GrapeFruit:
   * RGB (sRGB)
   * HSL
   * HSV
   * YIQ
   * YUV
   * CIE-XYZ
   * CIE-LAB (with the illuminant you want)
   * CMY
   * CMYK
   * HTML/CSS color definition (#RRGGBB, #RGB or the X11 color name)
   * RYB (artistic color wheel)
 .
 This package installs the library for Python 2.

Package: python3-grapefruit
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}
Suggests: python-grapefruit-doc
Description: Python module to manipulate color information easily (Python 3)
 GrapeFruit is a pure Python module that lets you easily manipulate and convert
 color information. Its primary goal is to be natural and flexible.
 .
 The following color systems are supported by GrapeFruit:
   * RGB (sRGB)
   * HSL
   * HSV
   * YIQ
   * YUV
   * CIE-XYZ
   * CIE-LAB (with the illuminant you want)
   * CMY
   * CMYK
   * HTML/CSS color definition (#RRGGBB, #RGB or the X11 color name)
   * RYB (artistic color wheel)
 .
 This package installs the library for Python 3.

Package: python-grapefruit-doc
Section: doc
Architecture: all
Depends: ${sphinxdoc:Depends}, ${misc:Depends}
Recommends: python-grapefruit
Description: Python module to manipulate color information easily - documentation
 GrapeFruit is a pure Python module that let you easily manipulate and convert
 color information. Its primary goal is to be natural and flexible.
 .
 This package provides the documentation for GrapeFruit.
